FROM openjdk:11

ADD ./target/lucky.jar /home/lucky/app.jar

EXPOSE 8080

ENTRYPOINT ["java","-jar","/home/lucky/app.jar"]

MAINTAINER Dimples_YJ 2890841438@qq.com