package top.dimples.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * @author Dimples_YJ
 * @date 2021/12/10
 */
@Data
public class GithubDTO {

    @JsonProperty(value = "uuid")
    private String id;

    @JsonProperty(value = "username")
    private String userName;

    @JsonProperty(value = "nickname")
    private String nickName;

    @JsonProperty(value = "avatar")
    private String avatarUrl;

    @JsonProperty(value = "email")
    private String email;

}
