package top.dimples.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * @author Dimples_YJ
 * @date 2021/12/10
 */
@Data
public class GiteeDTO {

    private String id;

    @JsonProperty(value = "login")
    private String userName;

    @JsonProperty(value = "name")
    private String nickName;

    @JsonProperty(value = "avatar_url")
    private String avatarUrl;

    private String email;

}
