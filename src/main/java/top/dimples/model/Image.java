package top.dimples.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

/**
 * @author Dimples_YJ
 * @date 2021/12/9
 */
@ApiModel(value="image")
@Data
@EqualsAndHashCode(callSuper=true)
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "image")
public class Image extends BaseModel {
    /**
     * 编号
     */
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    @ApiModelProperty(value="编号")
    private String id;

    /**
     * 本地路径
     */
    @TableField(value = "local_url")
    @ApiModelProperty(value = "本地保存路径")
    private String localUrl;

    /**
     * 路径
     */
    @TableField(value = "url")
    @ApiModelProperty(value="路径")
    private String url;

    /**
     * 图片类型
     */
    @TableField(value = "`type`")
    @ApiModelProperty(value="图片类型")
    private String type;

    public static final String COL_ID = "id";

    public static final String COL_URL = "url";

    public static final String COL_LOCAL_URL = "local_url";

    public static final String COL_TYPE = "type";

    public static final String COL_IS_DELETED = "is_deleted";

    public static final String COL_CREATE_TIME = "create_time";

    public static final String COL_UPDATE_TIME = "update_time";

    public static final String COL_REMARK = "remark";
}