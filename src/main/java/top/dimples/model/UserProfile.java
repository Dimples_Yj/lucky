package top.dimples.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@ApiModel(value="user_profile")
@Data
@EqualsAndHashCode(callSuper=true)
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "user_profile")
public class UserProfile extends BaseModel {
    /**
     * 用户编号
     */
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    @ApiModelProperty(value="用户编号")
    private String id;

    /**
     * 用户名
     */
    @TableField(value = "username")
    @ApiModelProperty(value="用户名")
    private String username;

    /**
     * 用户头像
     */
    @TableField(value = "avatar_url")
    @ApiModelProperty(value = "用户头像")
    private String avatarUrl;

    /**
     * 密码
     */
    @TableField(value = "`password`")
    @ApiModelProperty(value="密码")
    private String password;

    /**
     * 用户邮箱
     */
    @TableField(value = "email")
    @ApiModelProperty(value="用户邮箱")
    private String email;

    /**
     * 用户手机
     */
    @TableField(value = "phone")
    @ApiModelProperty(value="用户手机")
    private String phone;

    /**
     * 省
     */
    @TableField(value = "province")
    @ApiModelProperty(value="省")
    private String province;

    /**
     * 市
     */
    @TableField(value = "city")
    @ApiModelProperty(value="市")
    private String city;

    /**
     * 区
     */
    @TableField(value = "area")
    @ApiModelProperty(value="区")
    private String area;

    /**
     * 详细地址
     */
    @TableField(value = "address")
    @ApiModelProperty(value="详细地址")
    private String address;

    /**
     * 支付宝账号
     */
    @TableField(value = "alipay_account")
    @ApiModelProperty(value="支付宝账号")
    private String alipayAccount;

    /**
     * 微信账号
     */
    @TableField(value = "wx_account")
    @ApiModelProperty(value="微信账号")
    private String wxAccount;

    /**
     * 状态(1 正常、2 冻结)
     */
    @TableField(value = "`status`")
    @ApiModelProperty(value="状态(1 正常、2 冻结)")
    private Integer status;

    /**
     * 用户昵称
     */
    @TableField(value = "nick_name")
    @ApiModelProperty(value="用户昵称")
    private String nickName;

    public static final String COL_ID = "id";

    public static final String COL_USERNAME = "username";

    public static final String COL_PASSWORD = "password";

    public static final String COL_EMAIL = "email";

    public static final String COL_PHONE = "phone";

    public static final String COL_PROVINCE = "province";

    public static final String COL_CITY = "city";

    public static final String COL_AREA = "area";

    public static final String COL_ADDRESS = "address";

    public static final String COL_ALIPAY_ACCOUNT = "alipay_account";

    public static final String COL_WX_ACCOUNT = "wx_account";

    public static final String COL_STATUS = "status";

    public static final String COL_IS_DELETED = "is_deleted";

    public static final String COL_CREATE_TIME = "create_time";

    public static final String COL_UPDATE_TIME = "update_time";

    public static final String COL_NICK_NAME = "nick_name";
}