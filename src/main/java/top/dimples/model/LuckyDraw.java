package top.dimples.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author Dimples_Yj
 * @package top.dimples.model
 * @date 2021/12/18
 */
@ApiModel(value = "lucky_draw")
@Data
@EqualsAndHashCode(callSuper = true)
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "lucky_draw")
public class LuckyDraw extends BaseModel {
    /**
     * 编号
     */
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "编号")
    private String id;

    /**
     * 所属用户
     */
    @TableField(value = "user_id")
    @ApiModelProperty(value = "所属用户")
    private String userId;

    /**
     * 发起时间
     */
    @TableField(value = "start_time")
    @ApiModelProperty(value = "发起时间")
    private Date startTime;

    /**
     * 结束时间
     */
    @TableField(value = "end_time")
    @ApiModelProperty(value = "结束时间")
    private Date endTime;

    /**
     * 状态（0 审核中、1 进行中、2 已结束、3 审核失败）
     */
    @TableField(value = "`status`")
    @ApiModelProperty(value = "状态（0 审核中、1 进行中、2 已结束、3 审核失败）")
    private Integer status;

    /**
     * 标题
     */
    @TableField(value = "`title`")
    @ApiModelProperty(value = "标题")
    private String title;

    /**
     * 抽奖次数
     */
    @TableField(value = "`count`")
    @ApiModelProperty(value = "抽奖次数")
    private Integer count = 1;

    public static final String COL_ID = "id";

    public static final String COL_USER_ID = "user_id";

    public static final String COL_START_TIME = "start_time";

    public static final String COL_END_TIME = "end_time";

    public static final String COL_STATUS = "status";

    public static final String COL_COUNT = "count";

    public static final String COL_IS_DELETED = "is_deleted";

    public static final String COL_REMARK = "remark";

    public static final String COL_CREATE_TIME = "create_time";

    public static final String COL_UPDATE_TIME = "update_time";
}