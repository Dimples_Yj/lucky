package top.dimples.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @package top.dimples.model
 * @author Dimples_Yj
 * @date 2021/12/18
 */
@ApiModel(value="lucky_win")
@Data
@EqualsAndHashCode(callSuper=true)
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "lucky_win")
public class LuckyWin extends BaseModel {
    /**
     * 编号
     */
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    @ApiModelProperty(value="编号")
    private String id;

    /**
     * 中奖用户编号
     */
    @TableField(value = "user_id")
    @ApiModelProperty(value="中奖用户编号")
    private String userId;

    /**
     * 活动编号
     */
    @TableField(value = "lucky_id")
    @ApiModelProperty(value="活动编号")
    private String luckyId;

    /**
     * 奖品编号
     */
    @TableField(value = "prize_id")
    @ApiModelProperty(value="奖品编号")
    private String prizeId;


    /**
     * 中奖状态
     */
    @TableField(value = "`status`")
    @ApiModelProperty(value="中奖状态")
    private Integer status;

    public static final String COL_ID = "id";

    public static final String COL_USER_ID = "user_id";

    public static final String COL_LUCKY_ID = "lucky_id";

    public static final String COL_PRIZE_ID = "prize_id";

    public static final String COL_STATUS = "status";

    public static final String COL_IS_DELETED = "is_deleted";

    public static final String COL_REMARK = "remark";

    public static final String COL_CREATE_TIME = "create_time";

    public static final String COL_UPDATE_TIME = "update_time";
}