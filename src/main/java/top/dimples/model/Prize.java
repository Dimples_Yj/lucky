package top.dimples.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @package top.dimples.model
 * @author Dimples_Yj
 * @date 2021/12/18
 */
@ApiModel(value="prize")
@Data
@EqualsAndHashCode(callSuper=true)
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "prize")
public class Prize extends BaseModel {
    /**
     * 编号
     */
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    @ApiModelProperty(value="编号")
    private String id;

    /**
     * 所属抽奖活动
     */
    @TableField(value = "lucky_id")
    @ApiModelProperty(value="所属抽奖活动")
    private String luckyId;

    /**
     * 奖品数量
     */
    @TableField(value = "`number`")
    @ApiModelProperty(value="奖品数量")
    private Integer number;

    /**
     * 中奖率
     */
    @TableField(value = "probability")
    @ApiModelProperty(value="中奖率")
    private BigDecimal probability;

    /**
     * 奖品名称
     */
    @TableField(value = "prize_name")
    @ApiModelProperty(value="奖品名称")
    private String prizeName;

    /**
     * 奖品等级
     */
    @TableField(value = "`text`")
    @ApiModelProperty(value="奖品等级")
    private String text;


    /**
     * 奖品等级
     */
    @TableField(value = "img_url")
    @ApiModelProperty(value="奖品图片")
    private String imgUrl;

    public static final String COL_ID = "id";

    public static final String COL_LUCKY_ID = "lucky_id";

    public static final String COL_NUMBER = "number";

    public static final String COL_PROBABILITY = "probability";

    public static final String COL_PRIZE_NAME = "prize_name";

    public static final String COL_TEXT = "text";

    public static final String COL_IMG_URL = "img_url";

    public static final String COL_IS_DELETED = "is_deleted";

    public static final String COL_REMARK = "remark";

    public static final String COL_CREATE_TIME = "create_time";

    public static final String COL_UPDATE_TIME = "update_time";
}