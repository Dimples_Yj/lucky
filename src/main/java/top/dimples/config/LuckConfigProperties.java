package top.dimples.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author Dimples_Yj
 * @package top.dimples.config
 * @date 2021/12/12
 */
@Data
@Component
@ConfigurationProperties(prefix = "lucky")
public class LuckConfigProperties {

    private String authLocation;

    private String failLocation;

    private String forgetLocation;

}
