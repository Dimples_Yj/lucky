package top.dimples.service;

import top.dimples.common.ApiResponse;

/**
 * @author Dimples_YJ
 * @date 2021/12/5
 */
public interface CaptchaService {
    /**
     * 生成验证码
     */
    ApiResponse generateCode();

    /**
     * 验证验证码码
     */
    Boolean validateCode(String key,String code);

    /**
     * 删除验证码
     */
    Boolean removeCode(String key);

}
