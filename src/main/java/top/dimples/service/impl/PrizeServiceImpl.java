package top.dimples.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import top.dimples.mapper.PrizeMapper;
import top.dimples.model.Prize;
import top.dimples.service.PrizeService;

/**
 * @author Dimples_Yj
 * @package top.dimples.service.impl
 * @date 2021/12/18
 */
@Service
public class PrizeServiceImpl extends ServiceImpl<PrizeMapper, Prize> implements PrizeService {
}
