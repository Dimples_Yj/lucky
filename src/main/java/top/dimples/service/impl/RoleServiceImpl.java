package top.dimples.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import top.dimples.mapper.RoleMapper;
import top.dimples.model.Role;
import top.dimples.service.RoleService;

/**
 * @author Dimples_YJ
 * @date 2021/12/6
 */
@Service
@Transactional(rollbackFor = RuntimeException.class)
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements RoleService {
}
