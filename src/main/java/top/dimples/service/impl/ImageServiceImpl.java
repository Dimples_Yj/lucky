package top.dimples.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import top.dimples.mapper.ImageMapper;
import top.dimples.model.Image;
import top.dimples.service.ImageService;

import java.io.File;

/**
 * @author Dimples_YJ
 * @date 2021/12/9
 */
@Slf4j
@Service
public class ImageServiceImpl extends ServiceImpl<ImageMapper, Image> implements ImageService {

    @Override
    public Boolean deleteImageById(String id) {
        Image image = this.getById(id);
        File file = new File(image.getLocalUrl());
        if(file.exists() && file.isFile()){
            file.delete();
        }
        return this.removeById(image);
    }
}
