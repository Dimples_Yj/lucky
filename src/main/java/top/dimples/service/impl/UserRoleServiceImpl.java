package top.dimples.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import top.dimples.mapper.UserRoleMapper;
import top.dimples.model.UserRole;
import top.dimples.service.UserRoleService;

/**
 * @author Dimples_YJ
 * @date 2021/12/6
 */
@Service
@Transactional(rollbackFor = RuntimeException.class)
public class UserRoleServiceImpl extends ServiceImpl<UserRoleMapper, UserRole> implements UserRoleService {
}
