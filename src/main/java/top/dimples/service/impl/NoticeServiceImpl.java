package top.dimples.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import top.dimples.mapper.NoticeMapper;
import top.dimples.model.Notice;
import top.dimples.service.NoticeService;

/**
 * @author Dimples_Yj
 * @package top.dimples.service.impl
 * @date 2021/12/18
 */
@Service
public class NoticeServiceImpl extends ServiceImpl<NoticeMapper, Notice> implements NoticeService {
}
