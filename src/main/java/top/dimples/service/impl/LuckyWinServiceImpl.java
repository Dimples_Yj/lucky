package top.dimples.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.dimples.common.JWTUser;
import top.dimples.common.WinStatus;
import top.dimples.mapper.LuckyWinMapper;
import top.dimples.model.LuckyDraw;
import top.dimples.model.LuckyWin;
import top.dimples.model.Prize;
import top.dimples.model.UserProfile;
import top.dimples.service.LuckyDrawService;
import top.dimples.service.LuckyWinService;
import top.dimples.service.PrizeService;
import top.dimples.service.UserProfileService;
import top.dimples.vo.common.PageResult;
import top.dimples.vo.common.PageVO;
import top.dimples.vo.lucky.LuckWinListVO;
import top.dimples.vo.lucky.PrizeVO;
import top.dimples.vo.user.UserLuckyWinVO;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Dimples_Yj
 * @package top.dimples.service.impl
 * @date 2021/12/18
 */
@Service
public class LuckyWinServiceImpl extends ServiceImpl<LuckyWinMapper, LuckyWin> implements LuckyWinService {

    @Autowired
    private LuckyDrawService luckyDrawService;

    @Autowired
    private PrizeService prizeService;

    @Autowired
    private UserProfileService userProfileService;

    @Override
    public PageResult<LuckWinListVO> getUserAllLuckyWin(String userId, PageVO pageVO) {
        Page<LuckyWin> luckyWinPage = this.page(new Page<>(pageVO.getPageNum(), pageVO.getPageSize()), new LambdaQueryWrapper<LuckyWin>().eq(LuckyWin::getUserId, userId));
        return this.getLuckyWinListVO(luckyWinPage, pageVO, false);
    }

    @Override
    public PageResult<LuckWinListVO> getUserLuckyWin(String userId, PageVO pageVO) {
        Page<LuckyWin> luckyWinPage = this.page(
                new Page<>(pageVO.getPageNum(), pageVO.getPageSize()),
                new LambdaQueryWrapper<LuckyWin>()
                        .eq(LuckyWin::getUserId, userId)
                        .eq(LuckyWin::getStatus, WinStatus.WIN.getCode())
                        .orderByDesc(LuckyWin::getCreateTime));

        return this.getLuckyWinListVO(luckyWinPage, pageVO, true);
    }

    @Override
    public PageResult<UserLuckyWinVO> getAllWinUserInfo(JWTUser jwtUser, String drawId, PageVO pageVO) {
        // 1、检查当前请求的用户是不是创建者
        luckyDrawService.getOne(new LambdaQueryWrapper<LuckyDraw>()
                .eq(LuckyDraw::getUserId, jwtUser.getId())
                .eq(LuckyDraw::getId, drawId));
        // 2、分页查询中奖者的信息
        Page<LuckyWin> winPage = this.page(
                new Page<>(pageVO.getPageNum(), pageVO.getPageSize()),
                new LambdaQueryWrapper<LuckyWin>()
                        .eq(LuckyWin::getLuckyId, drawId)
                        .eq(LuckyWin::getStatus, WinStatus.WIN.getCode())
                        .orderByDesc(LuckyWin::getCreateTime)
        );
        List<LuckyWin> winList = winPage.getRecords();
        if (CollectionUtil.isNotEmpty(winList)) {
            List<String> userIds = winList.stream().map(LuckyWin::getUserId).distinct().collect(Collectors.toList());
            List<UserProfile> userProfiles = userProfileService.listByIds(userIds);
            List<UserLuckyWinVO> userLuckyWinVOList = userProfiles.stream().map(userProfile -> Convert.convert(UserLuckyWinVO.class, userProfile)).collect(Collectors.toList());
            return PageResult.of(userLuckyWinVOList, winPage.getSize(), winPage.getCurrent(), winPage.getTotal(), winPage.getPages());
        }
        return null;
    }

    private PageResult<LuckWinListVO> getLuckyWinListVO(Page<LuckyWin> luckyWinPage, PageVO pageVO, Boolean isFilter) {
        List<LuckyWin> luckyWinList = luckyWinPage.getRecords();
        if (CollectionUtil.isNotEmpty(luckyWinList)) {
            List<LuckWinListVO> pageList = null;

            if (!isFilter) {
                pageList = luckyWinList.stream().map(item -> {
                    LuckyDraw luckyDraw = luckyDrawService.getById(item.getLuckyId());
                    Prize prize = prizeService.getById(item.getPrizeId());
                    PrizeVO prizeVO = Convert.convert(PrizeVO.class, prize);
                    return LuckWinListVO.builder()
                            .winTime(item.getCreateTime())
                            .luckyDraw(luckyDraw)
                            .prizeVO(prizeVO)
                            .build();
                }).collect(Collectors.toList());
            } else {
                pageList = luckyWinList.stream().map(item -> {
                    LuckyDraw luckyDraw = luckyDrawService.getById(item.getLuckyId());
                    Prize prize = prizeService.getById(item.getPrizeId());
                    PrizeVO prizeVO = Convert.convert(PrizeVO.class, prize);
                    return LuckWinListVO.builder()
                            .winTime(item.getCreateTime())
                            .luckyDraw(luckyDraw)
                            .prizeVO(prizeVO)
                            .build();
                }).filter(luckWinListVO -> !StrUtil.containsAny(luckWinListVO.getPrizeVO().getText().trim(), "谢谢参与")).collect(Collectors.toList());
            }
            return PageResult.of(pageList, pageVO.getPageSize(), pageVO.getPageNum(), luckyWinPage.getTotal(), luckyWinPage.getPages());
        }
        return null;
    }

    /**
     * 获取当前用户参与的抽奖活动
     *
     * @param jwtUser 当前用户
     * @param pageVO  分页 VO 对象
     * @return 参与的抽奖活动
     */
    @Override
    public PageResult<LuckyDraw> getMyJoin(JWTUser jwtUser, PageVO pageVO) {
        Page<LuckyWin> page = this.page(new Page<>(pageVO.getPageNum(), pageVO.getPageSize()), new LambdaQueryWrapper<LuckyWin>().eq(LuckyWin::getUserId, jwtUser.getId()));
        List<LuckyWin> pageList = page.getRecords();
        if (CollectionUtil.isNotEmpty(pageList)) {
            List<LuckyDraw> drawList = luckyDrawService.listByIds(pageList.stream().map(LuckyWin::getLuckyId).distinct().collect(Collectors.toList()));
            return PageResult.of(drawList, pageVO.getPageSize(), pageVO.getPageNum(), page.getTotal(), page.getPages());
        }
        return null;
    }
}
