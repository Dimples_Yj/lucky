package top.dimples.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import top.dimples.model.Prize;
import top.dimples.model.UserProfile;
import top.dimples.service.BarkService;
import top.dimples.service.MailService;
import top.dimples.service.EmailManagerService;

import javax.mail.MessagingException;

/**
 * @author Dimples_YJ
 * @date 2021/12/7
 */
@Slf4j
@Service
public class EmailManagerServiceImpl implements EmailManagerService {

    /**
     * 邮件发送 service
     */
    @Autowired
    private MailService mailService;

    /**
     * thymeleaf 模版引擎
     */
    @Autowired
    private TemplateEngine templateEngine;


    /**
     * bark 通知开发的iPhone
     */
    @Autowired
    private BarkService barkService;

    @Async
    @Override
    public void sendRegisterEmail(UserProfile userProfile) {
        Context context = new Context();
        context.setVariable("nickName", userProfile.getNickName());
        String htmlMail = templateEngine.process("email/register", context);
        try {
            mailService.sendHtmlMail(userProfile.getEmail(), "欢迎注册Lucky!", htmlMail);
            barkService.notifyMyIPhone("好消息", "新用户注册啦！！！");
            log.info("用户注册,id:{},昵称:{},邮件发送成功", userProfile.getId(), userProfile.getEmail());
        } catch (MessagingException e) {
            log.error("用户注册,id:{},昵称:{},邮件发送失败", userProfile.getId(), userProfile.getEmail());
        }
    }

    @Async
    @Override
    public void sendWinEmail(UserProfile userProfile, Prize prize) {
        Context context = new Context();
        context.setVariable("nickName", userProfile.getNickName());
        context.setVariable("text", prize.getText());
        context.setVariable("prizeName", prize.getPrizeName());
        context.setVariable("prizeUrl", prize.getImgUrl());
        String htmlMail = templateEngine.process("email/luckywin", context);
        try {
            mailService.sendHtmlMail(userProfile.getEmail(), "恭喜中奖!", htmlMail);
            log.info("用户中奖,id:{},昵称:{},奖品:{},邮件发送成功", userProfile.getId(), userProfile.getEmail(), prize.getPrizeName());
        } catch (MessagingException e) {
            log.error("用户中奖,id:{},昵称:{},邮件发送失败", userProfile.getId(), userProfile.getEmail());
        }

    }

    @Async
    @Override
    public void sendFindPasswordEmail(String email, String forgetUrl) {
        Context context = new Context();
        context.setVariable("forgetUrl", forgetUrl);
        // todo： 修改找回密码的邮件模板
        String htmlMail = templateEngine.process("email/findPassword", context);
        try {
            mailService.sendHtmlMail(email, "找回密码!", htmlMail);
            log.info("找回密码,邮箱地址：{},邮件发送成功", email);
        } catch (MessagingException e) {
            log.error("找回密码,邮箱地址：{},邮件发送失败", email);
        }
    }
}
