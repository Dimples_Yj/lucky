package top.dimples.service;

import com.baomidou.mybatisplus.extension.service.IService;
import top.dimples.model.Prize;

/**
 * @author Dimples_Yj
 * @package top.dimples.service
 * @date 2021/12/18
 */
public interface PrizeService extends IService<Prize> {
}
