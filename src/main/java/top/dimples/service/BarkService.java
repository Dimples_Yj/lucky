package top.dimples.service;

/**
 * @author Dimples_YJ
 * @date 2021/12/7
 */
public interface BarkService {

    /**
     * 异步通知开发者的 iPhone
     *
     * @param title 通知的标题
     * @param msg   通知的消息内容
     * @return 是否通知成功
     */
    Boolean notifyMyIPhone(String title, String msg);

}
