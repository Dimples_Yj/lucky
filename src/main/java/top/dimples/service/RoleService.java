package top.dimples.service;

import com.baomidou.mybatisplus.extension.service.IService;
import top.dimples.model.Role;

/**
 * @author Dimples_YJ
 * @date 2021/12/6
 */
public interface RoleService extends IService<Role> {
}
