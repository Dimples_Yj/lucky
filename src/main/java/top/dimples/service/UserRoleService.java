package top.dimples.service;

import com.baomidou.mybatisplus.extension.service.IService;
import top.dimples.model.UserRole;

/**
 * @author Dimples_YJ
 * @date 2021/12/6
 */
public interface UserRoleService extends IService<UserRole> {
}
