package top.dimples.service;

import com.baomidou.mybatisplus.extension.service.IService;
import top.dimples.model.Image;

/**
 * @author Dimples_YJ
 * @date 2021/12/9
 */
public interface ImageService extends IService<Image> {

    Boolean deleteImageById(String id);

}
