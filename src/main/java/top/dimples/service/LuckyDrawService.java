package top.dimples.service;

import com.baomidou.mybatisplus.extension.service.IService;
import top.dimples.common.JWTUser;
import top.dimples.model.LuckyDraw;
import top.dimples.vo.common.PageResult;
import top.dimples.vo.common.PageVO;
import top.dimples.vo.lucky.LuckyCountVO;
import top.dimples.vo.lucky.LuckyDrawVO;

/**
 * @author Dimples_Yj
 * @package top.dimples.service
 * @date 2021/12/18
 */
public interface LuckyDrawService extends IService<LuckyDraw> {

    String addLuckyDraw(JWTUser jwtUser, LuckyDrawVO luckyDrawVO);

    LuckyDrawVO getLuckDrawInfo(String id);

    Integer lotteryIndex(JWTUser jwtUser,String id);

    LuckyCountVO getLuckyCount(JWTUser jwtUser);

    PageResult<LuckyDraw> getUserLuckyDraw(String userId, PageVO pageVO);

}
