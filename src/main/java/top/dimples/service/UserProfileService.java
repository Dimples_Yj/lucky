package top.dimples.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.swagger.annotations.Api;
import top.dimples.common.ApiResponse;
import top.dimples.model.UserProfile;
import top.dimples.vo.user.UserPasswordChangeVO;
import top.dimples.vo.user.UserRegisterVO;

public interface UserProfileService extends IService<UserProfile> {

    ApiResponse userRegister(UserRegisterVO userRegisterVO);

    ApiResponse getUserInfoById(String id);

    ApiResponse changeUserPassword(String uId,UserPasswordChangeVO userPasswordChangeVO);

    ApiResponse login(UserProfile userProfile);

    ApiResponse forgetPassword(String email);

    ApiResponse changeFindPassword(String key,String password);

    ApiResponse checkChangePwdKey(String key);

}
