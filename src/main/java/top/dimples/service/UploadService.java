package top.dimples.service;

import org.springframework.web.multipart.MultipartFile;
import top.dimples.common.ApiResponse;

/**
 * @author Dimples_YJ
 * @date 2021/12/9
 */
public interface UploadService {

    ApiResponse localUpload(MultipartFile file,String type);

}
