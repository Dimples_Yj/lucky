package top.dimples.service;

import top.dimples.model.Prize;
import top.dimples.model.UserProfile;

/**
 * @author Dimples_YJ
 * @date 2021/12/7
 */
public interface EmailManagerService {

    void sendRegisterEmail(UserProfile userProfile);

    void sendWinEmail(UserProfile userProfile, Prize prize);

    void sendFindPasswordEmail(String email,String forgetUrl);
}
