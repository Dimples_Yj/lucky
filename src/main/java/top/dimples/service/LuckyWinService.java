package top.dimples.service;

import com.baomidou.mybatisplus.extension.service.IService;
import top.dimples.common.JWTUser;
import top.dimples.model.LuckyDraw;
import top.dimples.model.LuckyWin;
import top.dimples.model.UserProfile;
import top.dimples.vo.common.PageResult;
import top.dimples.vo.common.PageVO;
import top.dimples.vo.lucky.LuckWinListVO;
import top.dimples.vo.lucky.LuckyDrawVO;
import top.dimples.vo.user.UserLuckyWinVO;

/**
 * @author Dimples_Yj
 * @package top.dimples.service
 * @date 2021/12/18
 */
public interface LuckyWinService extends IService<LuckyWin> {

    PageResult<LuckWinListVO> getUserAllLuckyWin(String userId, PageVO pageVO);

    PageResult<LuckWinListVO> getUserLuckyWin(String userId, PageVO pageVO);

    PageResult<UserLuckyWinVO> getAllWinUserInfo(JWTUser jwtUser, String drawId, PageVO pageVO);

    PageResult<LuckyDraw> getMyJoin(JWTUser jwtUser, PageVO pageVO);
}
