package top.dimples.vo.common;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author Dimples_YJ
 * @date 2021/12/10
 */
@ApiModel(value = "分页VO",description = "PageVO")
@Data
public class PageVO {

    /**
     * 当前页
     */
    @ApiModelProperty(value = "当前页")
    private Long pageNum = 1L;

    /**
     * 当前页大小
     */
    @ApiModelProperty(value = "当前页大小")
    private Long pageSize = 10L;
}
