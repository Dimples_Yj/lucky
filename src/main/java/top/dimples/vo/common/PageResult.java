package top.dimples.vo.common;

import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

/**
 * @author Dimples_YJ
 * @date 2021/12/10
 */
@AllArgsConstructor
@Data
@ApiModel(value = "分页响应对象", description = "PageResult")
public class PageResult<T> {

    @ApiModelProperty(value = "列表对象")
    private List<T> list;

    @ApiModelProperty(value = "分页大小")
    private Long pageSize;

    @ApiModelProperty(value = "当前页")
    private Long pageIndex;

    @ApiModelProperty(value = "总记录数")
    private Long totalRecords;

    @ApiModelProperty(value = "总页数")
    private Long totalPage;

    public static <T> PageResult<T> startPage(IPage<T> page) {
        return new PageResult<T>(page.getRecords(), page.getSize(), page.getCurrent(), page.getTotal(), page.getPages());
    }

    public static <T> PageResult<T> of(List<T> list, Long pageSize, Long pageIndex, Long totalRecords, Long totalPage) {
        return new PageResult<T>(list, pageSize, pageIndex, totalRecords, totalPage);
    }

}
