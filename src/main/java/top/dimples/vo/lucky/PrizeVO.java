package top.dimples.vo.lucky;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * @author Dimples_Yj
 * @package top.dimples.vo.lucky
 * @date 2021/12/18
 */
@Data
public class PrizeVO {

    @NotNull(message = "奖品数量不能为空")
    private Integer number;

    @NotNull(message = "奖品名字不能为空")
    private String prizeName;

    @NotNull(message = "奖品等级不能为空")
    private String text;

    @NotNull(message = "中奖率不能为空")
    private BigDecimal probability;

    @NotNull(message = "奖品图片不能为空")
    private String imgUrl;
}
