package top.dimples.vo.lucky;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Builder;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;


/**
 * @author Dimples_Yj
 * @package top.dimples.vo.lucky
 * @date 2021/12/18
 */
@Builder
@Data
public class LuckyDrawVO {

    @Valid
    @NotNull(message = "奖品不能为空")
    private List<PrizeVO> prizes;

    @NotNull(message = "开始时间不能为空")
    private Date startTime;

    @NotNull(message = "结束时间不能为空")
    private Date endTime;

    @NotNull(message = "标题不能为空")
    private String title;

    private Integer count;
}
