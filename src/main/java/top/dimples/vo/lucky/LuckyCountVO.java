package top.dimples.vo.lucky;

import lombok.Builder;
import lombok.Data;

/**
 * @author Dimples_YJ
 * @date 2021/12/18
 */
@Builder
@Data
public class LuckyCountVO {

    /**
     * 总的抽奖记录数量
     */
    private Long records;

    /**
     * 我发起的抽奖数量
     */
    private Long currents;

    /**
     * 中奖的数量
     */
    private Long wins;

    /**
     * 未读通知数量
     */
    private Long notices;

    /**
     * 参与的抽奖活动
     */
    private Long joins;


}
