package top.dimples.vo.lucky;

import lombok.Builder;
import lombok.Data;
import top.dimples.model.LuckyDraw;

import java.util.Date;

/**
 * @author Dimples_YJ
 * @date 2021/12/20
 */
@Builder
@Data
public class LuckWinListVO {

    /**
     * 中间时间
     */
    private Date winTime;

    /**
     * 中奖活动
     */
    private LuckyDraw luckyDraw;

    /**
     * 奖品
     */
    private PrizeVO prizeVO;

}
