package top.dimples.vo.user;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author Dimples_YJ
 * @date 2021/12/7
 */
@Data
@ApiModel(value = "用户修改密码VO", description = "UserPasswordChangeVO")
public class UserPasswordChangeVO {

    @ApiModelProperty(value = "验证码的key", required = true)
    @NotBlank(message = "验证码的 key 不能为空")
    private String key;

    @ApiModelProperty(value = "验证码", required = true)
    @NotBlank(message = "验证码的不能为空")
    private String code;

    @ApiModelProperty(value = "原密码", required = true)
    @NotBlank(message = "原密码不能为空")
    private String oPassword;

    @ApiModelProperty(value = "新密码", required = true)
    @NotBlank(message = "新密码不能为空")
    private String nPassword;

}
