package top.dimples.vo.user;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import top.dimples.vo.common.PageVO;

/**
 * @author Dimples_YJ
 * @date 2021/12/14
 */
@ApiModel(value = "用户分页对象",description = "UserListVO")
@Data
public class UserListVO extends PageVO {

    @ApiModelProperty(value = "用户昵称")
    private String nickName;


}
