package top.dimples.vo.user;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import top.dimples.model.Role;

import java.util.List;

/**
 * @author Dimples_YJ
 * @date 2021/12/5
 */
@Data
@ApiModel(value = "用户信息VO",description = "UserProfileVO")
public class UserProfileVO {

    /**
     * 用户编号
     */
    @ApiModelProperty(value="用户编号")
    private String id;

    /**
     * 用户名
     */
    @ApiModelProperty(value="用户名")
    private String username;

    /**
     * 用户头像
     */
    @ApiModelProperty(value = "用户头像")
    private String avatarUrl;

    /**
     * 用户邮箱
     */
    @ApiModelProperty(value="用户邮箱")
    private String email;

    /**
     * 用户手机
     */
    @ApiModelProperty(value="用户手机")
    private String phone;

    /**
     * 省
     */
    @ApiModelProperty(value="省")
    private String province;

    /**
     * 市
     */
    @ApiModelProperty(value="市")
    private String city;

    /**
     * 区
     */
    @ApiModelProperty(value="区")
    private String area;

    /**
     * 详细地址
     */
    @ApiModelProperty(value="详细地址")
    private String address;

    /**
     * 支付宝账号
     */
    @ApiModelProperty(value="支付宝账号")
    private String alipayAccount;

    /**
     * 微信账号
     */
    @ApiModelProperty(value="微信账号")
    private String wxAccount;

    /**
     * 状态(1 正常、2 冻结)
     */
    @ApiModelProperty(value="状态(1 正常、2 冻结)")
    private Integer status;

    /**
     * 用户昵称
     */
    @ApiModelProperty(value="用户昵称")
    private String nickName;

    /**
     * 用户角色
     */
    @ApiModelProperty(value = "用户角色")
    private List<Role> roles;

}
