package top.dimples.vo.user;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import top.dimples.model.BaseModel;

import javax.validation.constraints.NotBlank;

@Data
@ApiModel(value = "用户注册VO", description = "UserRegisterVO")
public class UserRegisterVO extends BaseModel {

    /**
     * 用户名
     */
    @ApiModelProperty(value = "用户名", required = true)
    @NotBlank(message = "用户名不能为空")
    private String username;

    /**
     * 密码
     */
    @ApiModelProperty(value = "密码", required = true)
    @NotBlank(message = "密码不能为空")
    private String password;

    /**
     * 用户邮箱
     */
    @ApiModelProperty(value = "用户邮箱", required = true)
    @NotBlank(message = "邮箱不能为空")
    private String email;

    /**
     * 用户手机
     */
    @ApiModelProperty(value = "用户手机", required = false)
    private String phone;

    /**
     * 省
     */
    @ApiModelProperty(value = "省", required = false)
    private String province;

    /**
     * 市
     */
    @ApiModelProperty(value = "市", required = false)
    private String city;

    /**
     * 区
     */
    @ApiModelProperty(value = "区", required = false)
    private String area;

    /**
     * 详细地址
     */
    @ApiModelProperty(value = "详细地址", required = false)
    private String address;

    /**
     * 支付宝账号
     */
    @ApiModelProperty(value = "支付宝账号", required = false)
    private String alipayAccount;

    /**
     * 微信账号
     */
    @ApiModelProperty(value = "微信账号", required = false)
    private String wxAccount;


    /**
     * 用户昵称
     */
    @ApiModelProperty(value = "用户昵称", required = false)
    private String nickName;
}
