package top.dimples.vo.user;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@ApiModel(value = "用户登陆VO",description = "UserLoginVO")
public class UserLoginVO {

    /**
     * 用户名
     */
    @ApiModelProperty(value = "用户名",required = true)
    @NotBlank(message = "用户名不能为空")
    private String username;

    /**
     * 密码
     */
    @ApiModelProperty(value = "密码",required = true)
    @NotBlank(message = "密码不能为空")
    private String password;

    /**
     * 验证码
     */
    @ApiModelProperty(value = "验证码",required = true)
    @NotBlank(message = "验证码不能为空")
    private String code;


    /**
     * 验证码的 key
     */
    @ApiModelProperty(value = "验证码的key", required = true)
    @NotBlank(message = "验证的key不能为空")
    private String key;

}
