package top.dimples.vo.user;

import lombok.Data;

/**
 * @author Dimples_Yj
 * @package top.dimples.vo.user
 * @date 2021/12/20
 */
@Data
public class UserLuckyWinVO {

    private String username;

    private String nickName;

    private String phone;

    private String province;

    private String city;

    private String area;

    private String address;

}
