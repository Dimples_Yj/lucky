package top.dimples.vo.role;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import top.dimples.vo.common.PageVO;

/**
 * @author Dimples_YJ
 * @date 2021/12/14
 */
@Data
@ApiModel(value = "角色分页VO",description = "RoleListVO")
public class RoleListVO extends PageVO {

    @ApiModelProperty(value = "角色字符串")
    private String roleName;

}
