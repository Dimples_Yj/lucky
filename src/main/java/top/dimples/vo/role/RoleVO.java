package top.dimples.vo.role;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author Dimples_YJ
 * @date 2021/12/14
 */
@ApiModel(value = "角色VO",description = "RoleVO")
@Data
public class RoleVO {

    @ApiModelProperty(value = "角色名称",required = true)
    private String roleName;

    @ApiModelProperty(value = "角色字符串",required = true)
    private String roleString;

    @ApiModelProperty(value = "备注",required = false)
    private String remark;

}
