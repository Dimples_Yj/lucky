package top.dimples.vo.notice;

import lombok.Builder;
import lombok.Data;

import java.util.Date;

/**
 * @author Dimples_Yj
 * @package top.dimples.vo.notice
 * @date 2021/12/20
 */
@Builder
@Data
public class NoticeListVO {
    /**
     * 通知时间
     */
    private Date createTime;

    /**
     * 通知内容
     */
    private String message;

    /**
     * 消息状态
     */
    private Integer status;

    /**
     * 消息状态文字
     */
    private String statusName;


}
