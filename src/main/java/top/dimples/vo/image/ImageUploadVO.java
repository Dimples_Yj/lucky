package top.dimples.vo.image;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author Dimples_YJ
 * @date 2021/12/9
 */
@Data
@ApiModel(value = "图片上传VO",description = "ImageUploadVO")
public class ImageUploadVO {

    /**
     * 编号
     */
    @ApiModelProperty(value="编号")
    private String id;

    /**
     * 路径
     */
    @ApiModelProperty(value="路径")
    private String url;

    /**
     * 图片类型
     */
    @ApiModelProperty(value="图片类型")
    private String type;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    private Date createTime;
}
