package top.dimples.vo.image;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import top.dimples.vo.common.PageVO;

/**
 * 图片对象的分页
 *
 * @author Dimples_YJ
 * @date 2021/12/10
 */
@ApiModel(value = "图片对象的分页",description = "ImageListVO")
@Data
public class ImageListVO extends PageVO {

    /**
     * 排序字段
     */
    @ApiModelProperty(value = "排序字段")
    private String orderBy;

    /**
     * 升序或者降序，默认降序
     */
    @ApiModelProperty(value = "排序方式")
    private String asc = "desc";

}
