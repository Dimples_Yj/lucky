package top.dimples.task;

import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import top.dimples.common.LuckyStatus;
import top.dimples.model.LuckyDraw;
import top.dimples.service.LuckyDrawService;

import java.util.List;

/**
 * @author Dimples_Yj
 * @package top.dimples.task
 * @date 2021/12/25
 */
@Slf4j
@Component
public class LuckyDrawTask {

    @Autowired
    private LuckyDrawService luckyDrawService;


    /**
     * 每天 0点检查
     */
    @Scheduled(cron = "10 0 0 * * ? ")
    public void checkLuckyDrawStatus() {
        log.info("定时任务开始");
        List<LuckyDraw> list = luckyDrawService.list(new LambdaQueryWrapper<LuckyDraw>().eq(LuckyDraw::getStatus, LuckyStatus.UNDER_WAY.getCode()).le(LuckyDraw::getEndTime, DateUtil.now()));
        list.forEach(luckyDraw -> {
            luckyDraw.setStatus(LuckyStatus.COMPLETE.getCode());
        });
        log.info("定时任务结束");
        luckyDrawService.updateBatchById(list);
    }

}
