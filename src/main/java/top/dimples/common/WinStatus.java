package top.dimples.common;

import lombok.Getter;

/**
 * @author Dimples_Yj
 * @package top.dimples.common
 * @date 2021/12/18
 */
@Getter
public enum WinStatus implements IStatus {
    /**
     * 中奖
     */
    WIN(1, "中奖"),

    /**
     * 未中奖
     */
    LOSE(0, "未中奖");
    private Integer code;

    private String message;

    WinStatus(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public static WinStatus fromCode(Integer code) {
        WinStatus[] statuses = WinStatus.values();
        for (WinStatus status : statuses) {
            if (status.getCode().equals(code)) {
                return status;
            }
        }
        return LOSE;
    }
}
