package top.dimples.common.interceptor;

import cn.hutool.json.JSONUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.servlet.HandlerInterceptor;
import top.dimples.common.ApiResponse;
import top.dimples.common.Consts;
import top.dimples.common.Status;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.nio.charset.StandardCharsets;

/**
 * @author Dimples_Yj
 * @package top.dimples.common.interceptor
 * @date 2021/12/12
 */
@Slf4j
public class AdminInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        log.info("后台拦截器---client:{}", request.getHeader(Consts.CLIENT));
        if (Consts.ADMIN.equalsIgnoreCase(request.getHeader(Consts.CLIENT))) {
            return true;
        }
        response.setCharacterEncoding(StandardCharsets.UTF_8.name());
        response.setContentType("application/json;charset=utf-8");
        JSONUtil.toJsonStr(ApiResponse.of(Status.UNAUTHORIZED.getCode(), "请使用正确的客户端请求", null), response.getWriter());
        return false;
    }
}
