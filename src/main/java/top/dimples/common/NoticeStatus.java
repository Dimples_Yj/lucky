package top.dimples.common;

import lombok.Getter;

/**
 * @author Dimples_Yj
 * @package top.dimples.common
 * @date 2021/12/18
 */
@Getter
public enum NoticeStatus implements IStatus {
    /**
     * 通知消息状态：未读
     */
    UNREAD(0, "未读"),
    /**
     * 通知消息状态：已读
     */
    READ(1, "已读");

    private Integer code;

    private String message;

    NoticeStatus(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public static NoticeStatus fromCode(Integer code) {
        NoticeStatus[] statuses = NoticeStatus.values();
        for (NoticeStatus status : statuses) {
            if (status.getCode().equals(code)) {
                return status;
            }
        }
        return READ;
    }
}
