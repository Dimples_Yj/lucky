package top.dimples.common;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Dimples_YJ
 * @date 2021/12/7
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class JWTUser {

    /**
     * 用户编号
     */
    private String id;

    /**
     * 用户名
     */
    private String username;

    /**
     * 用户头像
     */
    private String avatarUrl;

    /**
     * 用户邮箱
     */
    private String email;

    /**
     * 用户手机
     */
    private String phone;

    /**
     * 省
     */
    private String province;

    /**
     * 市
     */
    private String city;

    /**
     * 区
     */
    private String area;

    /**
     * 详细地址
     */
    private String address;

    /**
     * 支付宝账号
     */
    private String alipayAccount;

    /**
     * 微信账号
     */
    private String wxAccount;

    /**
     * 状态(1 正常、2 冻结)
     */
    private Integer status;

    /**
     * 用户昵称
     */
    private String nickName;

    /**
     * 用户角色
     */
    private String roles;


}
