package top.dimples.common;

import lombok.Getter;

/**
 * @author Dimples_Yj
 * @package top.dimples.common
 * @date 2021/12/18
 */
@Getter
public enum LuckyStatus implements IStatus {
    /**
     * 抽奖活动状态：审核中
     */
    UNDER_REVIEW(0,"审核中"),

    /**
     * 抽奖活动状态：进行中
     */
    UNDER_WAY(1,"进行中"),

    /**
     * 抽奖活动状态：已结束
     */
    COMPLETE(2,"已结束"),

    /**
     * 抽奖活动状态：审核失败
     */
    FAIL(3,"审核失败");

    private Integer code;

    private String message;

    LuckyStatus(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public String toString() {
        return String.format(" LuckyStatus:{code=%s, message=%s} ", getCode(), getMessage());
    }
}
