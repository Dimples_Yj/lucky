package top.dimples.common;

/**
 * <p>
 * 常量池
 * </p>
 *
 */
public interface Consts {
    /**
     * 启用
     */
    Integer ENABLE = 1;
    /**
     * 禁用
     */
    Integer DISABLE = 0;

    /**
     * 页面
     */
    Integer PAGE = 1;

    /**
     * 按钮
     */
    Integer BUTTON = 2;

    /**
     * JWT 在 Redis 中保存的key前缀
     */
    String REDIS_JWT_KEY_PREFIX = "security:jwt:";

    /**
     * 星号
     */
    String SYMBOL_STAR = "*";

    /**
     * 邮箱符号
     */
    String SYMBOL_EMAIL = "@";

    /**
     * 默认当前页码
     */
    Integer DEFAULT_CURRENT_PAGE = 1;

    /**
     * 默认每页条数
     */
    Integer DEFAULT_PAGE_SIZE = 10;

    /**
     * 匿名用户 用户名
     */
    String ANONYMOUS_NAME = "匿名用户";

    /**
     * API 验证码请求头
     */
    String AUTHORIZATION = "Authorization";

    /**
     * Bearer
     */
    String BEARER = "Bearer ";


    /**
     * JWT 密钥
     */
    String JWT_KEY = "Dimples_ZHOUAOJUN_fuxiaotian_chenjiajun";


    /**
     * 降序
     */
    String ASC = "asc";


    /**
     * 默认密码
     */
    String PASSWORD = "123456";


    /**
     * 客户端请求头
     */
    String CLIENT = "Client";


    /**
     * 前台
     */
    String WEB = "web";

    /**
     * 后台管理
     */
    String ADMIN = "admin";


}
