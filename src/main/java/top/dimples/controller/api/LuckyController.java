package top.dimples.controller.api;

import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;
import top.dimples.common.ApiResponse;
import top.dimples.common.JWTUser;
import top.dimples.common.Status;
import top.dimples.model.LuckyDraw;
import top.dimples.service.LuckyDrawService;
import top.dimples.service.LuckyWinService;
import top.dimples.vo.common.PageResult;
import top.dimples.vo.common.PageVO;
import top.dimples.vo.lucky.LuckWinListVO;
import top.dimples.vo.lucky.LuckyDrawVO;

import javax.validation.Valid;

/**
 * @author Dimples_Yj
 * @package top.dimples.controller.api
 * @date 2021/12/18
 */
@Slf4j
@Api(tags = "抽奖模块")
@RequestMapping("/api/lucky")
@RestController
public class LuckyController {

    @Autowired
    private LuckyDrawService luckyDrawService;

    @Autowired
    private LuckyWinService luckyWinService;

    @ApiOperation(value = "新增抽奖活动")
    @PostMapping("/add")
    public ApiResponse addLuckyDraw(@ApiIgnore JWTUser jwtUser, @RequestBody @Valid LuckyDrawVO luckyDrawVO) {
        return StrUtil.isNotEmpty(luckyDrawService.addLuckyDraw(jwtUser, luckyDrawVO)) ? ApiResponse.ofSuccess() : ApiResponse.ofStatus(Status.ERROR);
    }

    @ApiOperation(value = "获取活动详情")
    @GetMapping("/info/{id}")
    public ApiResponse getLuckyDrawInfo(@PathVariable("id") String id) {
        log.info("查询活动的id：{}", id);
        if (StrUtil.isNotEmpty(id)) {
            LuckyDrawVO luckyDrawVO = luckyDrawService.getLuckDrawInfo(id);
            if (ObjectUtil.isNotEmpty(luckyDrawVO)) {
                return ApiResponse.ofSuccess(luckyDrawVO);
            }
            return ApiResponse.of(Status.BAD_REQUEST.getCode(), "活动不存在", null);
        }
        return ApiResponse.of(Status.BAD_REQUEST.getCode(), "id不能为空", null);
    }


    @ApiOperation(value = "获取中奖项")
    @GetMapping("/lottery/{id}")
    public ApiResponse getLottery(@ApiIgnore JWTUser jwtUser, @PathVariable("id") String id) {
        if (StrUtil.isNotEmpty(id)) {
            Integer lotteryIndex = luckyDrawService.lotteryIndex(jwtUser, id);
            if (lotteryIndex > -1) {
                return ApiResponse.ofSuccess(
                        MapUtil.builder()
                                .put("index", lotteryIndex)
                                .build()
                );
            }
            if (lotteryIndex == -2) {
                return ApiResponse.of(Status.SUCCESS.getCode(), "抽奖达到上限", null);
            }
            return ApiResponse.of(Status.BAD_REQUEST.getCode(),   "奖项不存在或已经被抽完", null);
        }
        return ApiResponse.of(Status.BAD_REQUEST.getCode(), "id不能为空", null);
    }


    @ApiOperation(value = "抽奖统计")
    @GetMapping("/info")
    public ApiResponse getCountInfo(@ApiIgnore JWTUser jwtUser) {
        return ApiResponse.ofSuccess(luckyDrawService.getLuckyCount(jwtUser));
    }

    @ApiOperation(value = "获取当前用户发布的抽奖活动")
    @GetMapping("/mylucky")
    public ApiResponse getUserLuckyDraw(@ApiIgnore JWTUser jwtUser, PageVO pageVO) {
        PageResult<LuckyDraw> result = luckyDrawService.getUserLuckyDraw(jwtUser.getId(), pageVO);
        return ApiResponse.ofSuccess(result);
    }

    @ApiOperation(value = "获取当前用户所有的抽奖记录")
    @GetMapping("/myAllLuckyWin")
    public ApiResponse getUserAllLuckyWin(@ApiIgnore JWTUser jwtUser, PageVO pageVO) {
        PageResult<LuckWinListVO> result = luckyWinService.getUserAllLuckyWin(jwtUser.getId(), pageVO);
        if (ObjectUtil.isNotEmpty(result)) {
            return ApiResponse.ofSuccess(result);
        }
        return ApiResponse.of(Status.BAD_REQUEST.getCode(), "没有该数据", null);
    }

    @ApiOperation(value = "获取当前用户的中奖记录")
    @GetMapping("/myLuckyWin")
    public ApiResponse getUserLuckyWin(@ApiIgnore JWTUser jwtUser, PageVO pageVO) {
        PageResult<LuckWinListVO> result = luckyWinService.getUserLuckyWin(jwtUser.getId(), pageVO);
        if (ObjectUtil.isNotEmpty(result)) {
            return ApiResponse.ofSuccess(result);
        }
        return ApiResponse.of(Status.BAD_REQUEST.getCode(), "没有该数据", null);
    }

    @ApiOperation(value = "获取当前用户的参与的抽奖")
    @GetMapping("/getMyJoin")
    public ApiResponse getMyJoin(@ApiIgnore JWTUser jwtUser, PageVO pageVO) {
        PageResult<LuckyDraw> result = luckyWinService.getMyJoin(jwtUser, pageVO);
        if (ObjectUtil.isNotEmpty(result)) {
            return ApiResponse.ofSuccess(result);
        }
        return ApiResponse.of(Status.BAD_REQUEST.getCode(), "没有该数据", null);
    }


}
