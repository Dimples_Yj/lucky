package top.dimples.controller.common;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.dimples.common.ApiResponse;
import top.dimples.service.CaptchaService;

/**
 * @author Dimples_YJ
 * @date 2021/12/5
 */
@Api(tags = "验证码模块")
@Slf4j
@RequestMapping("/captcha")
@RestController
public class CaptchaController {


    @Autowired
    private CaptchaService captchaService;

    /**
     * 验证码
     *
     * @return 图形验证码
     */
    @ApiOperation(value = "获取验证码")
    @GetMapping("")
    public ApiResponse getCaptcha() {
        return captchaService.generateCode();
    }


}
