package top.dimples.controller.common;

import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import top.dimples.common.ApiResponse;
import top.dimples.service.UploadService;
import top.dimples.vo.image.ImageUploadVO;

/**
 * @author Dimples_YJ
 * @date 2021/12/9
 */
@Api(tags = "图片上传")
@RequestMapping("/upload")
@RestController
public class UploadController {

    @Autowired
    private UploadService uploadService;

    @ApiOperation(value = "用户上传图片接口",response = ImageUploadVO.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "type", value = "图片的作用,如:普通图片、用户头像等",paramType = "form",defaultValue = "普通图片")
    })
    @PostMapping("")
    public ApiResponse upload(@ApiParam(value = "图片二进制数据") @RequestParam("file") MultipartFile file, @RequestParam(value = "type", required = false) String type) {
        return uploadService.localUpload(file, type);
    }





}
