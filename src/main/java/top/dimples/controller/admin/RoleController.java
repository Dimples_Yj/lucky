package top.dimples.controller.admin;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import top.dimples.common.ApiResponse;
import top.dimples.common.Status;
import top.dimples.model.Role;
import top.dimples.service.RoleService;
import top.dimples.vo.role.RoleListVO;
import top.dimples.vo.role.RoleVO;

/**
 * @author Dimples_YJ
 * @date 2021/12/14
 */
@Api(tags = "角色模块")
@RestController
@RequestMapping("/admin/role")
public class RoleController {

    @Autowired
    private RoleService roleService;


    @ApiOperation(value = "分页获取角色信息")
    @GetMapping("/list")
    public ApiResponse getList(RoleListVO roleListVO) {
        Page<Role> page = roleService.page(
                new Page<>(roleListVO.getPageNum(),roleListVO.getPageSize()),
                new LambdaQueryWrapper<Role>()
                        .eq(StrUtil.isNotEmpty(roleListVO.getRoleName()),
                                Role::getRoleName,
                                roleListVO.getRoleName()));
        return ApiResponse.ofSuccess(page);
    }


    @ApiOperation(value = "新增角色")
    @PostMapping("/add")
    public ApiResponse addRole(@RequestBody RoleVO roleVO){
        return roleService.save(Convert.convert(Role.class,roleVO))
                ? ApiResponse.ofSuccess()
                : ApiResponse.ofStatus(Status.ERROR);
    }


}
