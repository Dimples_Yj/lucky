package top.dimples.controller.admin;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.dimples.anotation.Permission;
import top.dimples.common.ApiResponse;
import top.dimples.common.LuckyStatus;
import top.dimples.common.Status;
import top.dimples.model.LuckyDraw;
import top.dimples.service.LuckyDrawService;
import top.dimples.service.PrizeService;
import top.dimples.vo.common.PageResult;
import top.dimples.vo.common.PageVO;

/**
 * @author Dimples_Yj
 * @package top.dimples.controller.admin
 * @date 2021/12/23
 */
@Slf4j
@Api(tags = "抽奖活动管理模块")
@RequestMapping("/admin/lucky")
@RestController
public class LuckAdminController {

    @Autowired
    private LuckyDrawService luckyDrawService;

    @Autowired
    private PrizeService prizeService;


    @ApiOperation(value = "通过抽奖活动的审核")
    @Permission
    @GetMapping("/pass/{id}")
    public ApiResponse passLuckyDraw(@PathVariable("id") String id) {
        LuckyDraw luckyDraw = luckyDrawService.getById(id);
        luckyDraw.setStatus(LuckyStatus.UNDER_WAY.getCode());
        return luckyDrawService.updateById(luckyDraw)
                ? ApiResponse.ofSuccess()
                : ApiResponse.of(Status.BAD_REQUEST.getCode(), "操作失败", null);
    }

    @ApiOperation(value = "拒绝抽奖活动的审核")
    @Permission
    @GetMapping("/fail/{id}")
    public ApiResponse failLuckyDraw(@PathVariable("id") String id) {
        LuckyDraw luckyDraw = luckyDrawService.getById(id);
        luckyDraw.setStatus(LuckyStatus.FAIL.getCode());
        return luckyDrawService.updateById(luckyDraw)
                ? ApiResponse.ofSuccess()
                : ApiResponse.of(Status.BAD_REQUEST.getCode(), "操作失败", null);
    }

    @Permission
    @ApiOperation(value = "分页获取抽奖活动")
    @GetMapping("/list")
    public ApiResponse list(PageVO pageVO) {
        Page<LuckyDraw> page = luckyDrawService.page(new Page<>(pageVO.getPageNum(), pageVO.getPageSize()),new LambdaQueryWrapper<LuckyDraw>().orderByDesc(LuckyDraw::getCreateTime));
        return ApiResponse.ofSuccess(PageResult.startPage(page));
    }


}
