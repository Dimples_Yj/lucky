package top.dimples.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import top.dimples.model.Notice;

/**
 * @package top.dimples.mapper
 * @author Dimples_Yj
 * @date 2021/12/18
 */
@Mapper
public interface NoticeMapper extends BaseMapper<Notice> {
}