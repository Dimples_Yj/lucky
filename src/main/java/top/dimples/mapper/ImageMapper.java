package top.dimples.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import top.dimples.model.Image;

/**
 * @author Dimples_YJ
 * @date 2021/12/9
 */
@Mapper
public interface ImageMapper extends BaseMapper<Image> {
}