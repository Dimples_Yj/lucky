package top.dimples.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import top.dimples.model.UserProfile;

@Mapper
public interface UserProfileMapper extends BaseMapper<UserProfile> {
}