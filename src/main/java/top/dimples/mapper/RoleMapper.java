package top.dimples.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import top.dimples.model.Role;

/**
 * @author Dimples_YJ
 * @date 2021/12/6
 */
@Mapper
public interface RoleMapper extends BaseMapper<Role> {
}