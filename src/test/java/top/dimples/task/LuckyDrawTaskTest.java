package top.dimples.task;

import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import top.dimples.common.LuckyStatus;
import top.dimples.model.LuckyDraw;
import top.dimples.service.LuckyDrawService;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Dimples_Yj
 * @package top.dimples.task
 * @date 2021/12/25
 */
@SpringBootTest
class LuckyDrawTaskTest {

    @Autowired
    private LuckyDrawService luckyDrawService;


    @Test
    public void testTimeDiff() {
        List<LuckyDraw> list = luckyDrawService.list(new LambdaQueryWrapper<LuckyDraw>().eq(LuckyDraw::getStatus, LuckyStatus.UNDER_WAY.getCode()).le(LuckyDraw::getEndTime, DateUtil.parse("2021-12-18")));
        list.forEach(System.out::println);
    }


}