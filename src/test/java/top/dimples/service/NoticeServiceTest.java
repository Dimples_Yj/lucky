package top.dimples.service;

import cn.hutool.core.util.StrUtil;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import top.dimples.common.NoticeStatus;
import top.dimples.model.Notice;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Dimples_Yj
 * @package top.dimples.service
 * @date 2021/12/20
 */
@SpringBootTest
class NoticeServiceTest {

    @Autowired
    private NoticeService noticeService;

    @Test
    void testAdd() {
        Notice notice = Notice.builder()
                .status(NoticeStatus.UNREAD.getCode())
                .userId("1472049303236763650")
                .message("恭喜中奖！")
                .build();
        assertTrue(noticeService.save(notice));
    }

    @Test
    void testChar() {
        System.out.println(StrUtil.containsAny("谢谢参与", '谢', '参', '与'));
    }


}