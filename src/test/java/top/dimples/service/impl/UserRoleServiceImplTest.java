package top.dimples.service.impl;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import top.dimples.model.UserRole;
import top.dimples.service.UserRoleService;

/**
 * @author Dimples_YJ
 * @date 2021/12/7
 */
@SpringBootTest
class UserRoleServiceImplTest {

    @Autowired
    private UserRoleService userRoleService;


    @Test
    void addTest(){
        UserRole userRole = UserRole.builder()
                .roleId("1468233905512411138")
                .userId("1467379866839109633")
                .build();
        boolean save = userRoleService.save(userRole);
        Assertions.assertTrue(save);

    }

}