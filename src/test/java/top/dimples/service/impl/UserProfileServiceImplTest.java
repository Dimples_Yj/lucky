package top.dimples.service.impl;

import cn.hutool.core.convert.Convert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;
import top.dimples.common.ApiResponse;
import top.dimples.model.UserProfile;
import top.dimples.service.UserProfileService;
import top.dimples.vo.user.UserProfileVO;
import top.dimples.vo.user.UserRegisterVO;

import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * @author Dimples_YJ
 * @date 2021/12/5
 */
@SpringBootTest
class UserProfileServiceImplTest {

    @Autowired
    private UserProfileService userProfileService;

    @Test
    @Transactional
    void userRegiter() {
        UserRegisterVO userRegisterVO = new UserRegisterVO();
        userRegisterVO.setNickName("xiaoming");
        userRegisterVO.setUsername("xiao");
        userRegisterVO.setPassword("test");
        ApiResponse apiResponse = userProfileService.userRegister(userRegisterVO);
        assertNotNull(apiResponse.getData());
    }

    @Test
    void getUserInfoById() {
        String id = "1467379866839109633";
        ApiResponse apiResponse = userProfileService.getUserInfoById(id);
        assertNotNull(apiResponse.getData());
    }

    @Test
    void updateUserProfile() {
    }

    @Test
    void convertUserProfileVOTest() {
        UserProfileVO userProfileVO = new UserProfileVO();
        userProfileVO.setNickName("xiaoming");
        userProfileVO.setUsername("xiao");
        UserProfile userProfile = Convert.convert(UserProfile.class, userProfileVO);
        System.out.println("userProfile = " + userProfile);
    }
}