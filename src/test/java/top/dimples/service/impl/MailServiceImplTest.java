package top.dimples.service.impl;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import top.dimples.service.MailService;

import javax.mail.MessagingException;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Dimples_YJ
 * @date 2021/12/7
 */
@SpringBootTest
class MailServiceImplTest {

    @Autowired
    private MailService mailService;

    @Autowired
    private TemplateEngine templateEngine;

    @Test
    void sendSimpleMail() throws MessagingException {

        String email = templateEngine.process("email/register", new Context());

        // mailService.sendHtmlMail("1600139455@qq.com", "这是一封测试的邮件", email);
        mailService.sendHtmlMail("2890841438@qq.com", "这是一封测试的邮件", email);
    }
}