package top.dimples.service.impl;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import top.dimples.model.Role;
import top.dimples.service.RoleService;

/**
 * @author Dimples_YJ
 * @date 2021/12/6
 */
@SpringBootTest
class RoleServiceImplTest {

    @Autowired
    private RoleService roleService;

    @Test
    void testAdd(){
        Role role = Role.builder()
                .id("1468224439932977154")
                .roleName("超级管理员")
                .roleString("admin")
                .build();
        Assertions.assertTrue(roleService.save(role));
    }

}