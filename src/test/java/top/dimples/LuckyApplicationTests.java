package top.dimples;

import cn.hutool.core.util.ArrayUtil;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import top.dimples.service.UserProfileService;

import java.io.IOException;

@SpringBootTest
class LuckyApplicationTests {

    @Autowired
    private UserProfileService userProfileService;


    @Test
    void contextLoads() throws IOException {
        String[] per = {"admin","common"};
        String[] rol = {"admin"};
        Assertions.assertTrue(ArrayUtil.containsAny(per,rol));


    }

}
